package UnitsApi

import ClosureTimers
import HashMap
import HashSet
import HeroPreset
import ObjectIdGenerator
import Icons
import ItemObjEditing
import ObjectIds

import AbilitiesApi

public constant ID_SYLVANAS = 'H000'
public constant ID_JAINA = 'H001'
public constant ID_ZOMBIE = 'u000'
public constant ID_ZOMBIE_FAT = 'u001'
public constant ID_KATYDMA = 'U000'
public constant ID_KATYDMA_ALT = 'U001'

// Skeleton
// Skeleton Archer
// Skeleton Mage
// Mummy
// Unholy Corpse
// Frozen Terror
// Slave Master
// Katydma

public constant permanentItems = new HashSet<item>()

public function cleanupItem(unit forWhom, item ite)
    doAfter(60.) ->
        if not forWhom.hasItem(ite) and not permanentItems.has(ite)
            ite.remove()

let zombie_ids = [ID_ZOMBIE, ID_ZOMBIE, ID_ZOMBIE, ID_ZOMBIE, ID_ZOMBIE, ID_ZOMBIE_FAT]
public function randomZombieId() returns int
    return zombie_ids[GetRandomInt(0, 5)]

public class BowData
    string name
    string icon
    int rank
    construct(string name, string icon, int rank)
        this.name = name
        this.icon = icon
        this.rank = rank

    function power() returns int
        return this.rank * 5

public class MeleeData
    string name
    string icon
    int rank
    construct(string name, string icon, int rank)
        this.name = name
        this.icon = icon
        this.rank = rank

    function power() returns int
        return this.rank

public class WeaponQualifier
    string qualifier
    real bonus

    construct(string qualifier, real bonus)
        this.qualifier = qualifier
        this.bonus = bonus

public constant bows = new IterableMap<int, BowData>()
    ..put(compiletime(ITEM_ID_GEN.next()), new BowData("Bow", Icons.bTNImprovedBows, 1))
    ..put(compiletime(ITEM_ID_GEN.next()), new BowData("Crossbow", "ReplaceableTextures\\CommandButtons\\BTNAoECrossbows.blp", 2))
    ..put(compiletime(ITEM_ID_GEN.next()), new BowData("Composite Bow", "ReplaceableTextures\\CommandButtons\\BTNGoldenWoodBow.blp", 3))
    ..put(compiletime(ITEM_ID_GEN.next()), new BowData("Battle Bow", "ReplaceableTextures\\CommandButtons\\BTNEmpoweredBow.BLP", 4))
    ..put(compiletime(ITEM_ID_GEN.next()), new BowData("Hunters Bow", "ReplaceableTextures\\CommandButtons\\BTNSanguineBow.blp", 5))
    ..put(compiletime(ITEM_ID_GEN.next()), new BowData("War Bow", "ReplaceableTextures\\CommandButtons\\BTNInfernal bow.blp", 6))
    ..put(compiletime(ITEM_ID_GEN.next()), new BowData("Double Bow", "ReplaceableTextures\\CommandButtons\\BTNWoodenHornBow.blp", 7))
    ..put(compiletime(ITEM_ID_GEN.next()), new BowData("Edge Bow", "ReplaceableTextures\\CommandButtons\\BTNHunter 02.blp", 8))
    ..put(compiletime(ITEM_ID_GEN.next()), new BowData("Repeating Crossbow", "ReplaceableTextures\\CommandButtons\\BTNAoECrossbows.blp", 9))
    ..put(compiletime(ITEM_ID_GEN.next()), new BowData("Cedar Bow", "ReplaceableTextures\\CommandButtons\\BTNScoutsBow.blp", 10))
    ..put(compiletime(ITEM_ID_GEN.next()), new BowData("Crusader Bow", "ReplaceableTextures\\CommandButtons\\BTNHolyBow.BLP", 11))
    ..put(compiletime(ITEM_ID_GEN.next()), new BowData("Rune Bow", "ReplaceableTextures\\CommandButtons\\BTNImprovedBloodElvenBows.blp", 12))
    ..put(compiletime(ITEM_ID_GEN.next()), new BowData("Arbalest", "ReplaceableTextures\\CommandButtons\\BTNcrossbowGS.blp", 13))
    ..put(compiletime(ITEM_ID_GEN.next()), new BowData("Ballista", "ReplaceableTextures\\CommandButtons\\BTNcrossbowGS.blp", 14))
public constant bow_pool = new HashList<int>()

public constant melees = new IterableMap<int, MeleeData>()
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Sword", Icons.bTNSteelMelee, 1))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Longsword", Icons.bTNSteelMelee, 2))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Pickaxe", Icons.bTNGatherGold, 3))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Axe", Icons.bTNSpiritWalkerAdeptTraining, 4))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Club", Icons.bTNAlleriaFlute, 5))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Dagger", Icons.bTNDaggerOfEscape, 6))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Hammer", Icons.bTNHammer, 7))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Spear", Icons.bTNSteelRanged, 8))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Javelin", Icons.bTNImpalingBolt, 9))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Mace", Icons.bTNStaffOfSanctuary, 10))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Scepter", Icons.bTNStaffOfSanctuary, 11))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Katar", Icons.bTNClawsOfAttack, 12))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Polearm", Icons.bTNThoriumRanged, 13))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Staff", Icons.bTNStaffOfSilence, 14))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Hatchet", Icons.bTNOrcMeleeUpTwo, 15))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Double-axe", Icons.bTNOrcMeleeUpOne, 16))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Hand Axe", Icons.bTNSpiritWalkerAdeptTraining, 17))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Large Axe", Icons.bTNSpiritWalkerAdeptTraining, 18))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Broad Axe", Icons.bTNSpiritWalkerAdeptTraining, 19))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Battle Axe", Icons.bTNSpiritWalkerAdeptTraining, 20))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Great Axe", Icons.bTNSpiritWalkerAdeptTraining, 21))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Giant Axe", Icons.bTNSpiritWalkerAdeptTraining, 22))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Tomahawk", Icons.bTNSpiritWalkerAdeptTraining, 23))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Berserker Axe", Icons.bTNSpiritWalkerAdeptTraining, 24))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Champion Axe", Icons.bTNSpiritWalkerAdeptTraining, 25))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Glorious Axe", Icons.bTNSpiritWalkerAdeptTraining, 26))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Twin Axe", Icons.bTNOrcMeleeUpOne, 27))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Cleaver", Icons.bTNOrcMeleeUpOne, 28))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Military Axe", Icons.bTNSpiritWalkerAdeptTraining, 29))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Ancient Axe", Icons.bTNSpiritWalkerMasterTraining, 30))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Spiked Club", Icons.bTNDivineShieldOff, 31))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Barbed Club", Icons.bTNDivineShieldOff, 32))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Tyrant Club", Icons.bTNDivineShieldOff, 33))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Dirk", Icons.bTNSacrifice, 34))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Blade", Icons.bTNSacrifice, 35))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Stiletto", Icons.bTNSacrifice, 36))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Bone Knife", Icons.bTNCleavingAttack, 37))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Spike", Icons.bTNQuillSprayOff, 38))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Maul", Icons.bTNChime, 39))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Warhammer", Icons.bTNHumanBuild, 40))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Battle Hammer", Icons.bTNHammer, 41))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("War Club", Icons.bTNChime, 42))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Mallet", Icons.bTNAdvStruct, 43))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Glaive", Icons.bTNUpgradeMoonGlaive, 44))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Harpoon", Icons.bTNNagaWeaponUp2, 45))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Cestus", Icons.bTNImprovedStrengthOfTheWild, 46))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Wrist Sword", Icons.bTNTheBlackArrow, 47))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Claws", Icons.bTNGhoulFrenzy, 48))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Talons", Icons.bTNGhoulFrenzy, 49))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Morning Star", "ReplaceableTextures\\CommandButtons\\BTNBloodLineMorningStar.blp", 50))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Flail", "ReplaceableTextures\\CommandButtons\\BTNBloodLineMorningStar.blp", 51))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Flanged Mace", "ReplaceableTextures\\CommandButtons\\BTNBloodLineMorningStar.blp", 52))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Knout", Icons.bTNWitchDoctorAdept, 53))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Scourge", Icons.bTNGhoulFrenzy, 54))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Bardiche", Icons.bTNOrcMeleeUpThree, 55))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Scythe", Icons.bTNOrcMeleeUpOne, 56))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Voulge", Icons.bTNDaggerOfEscape, 57))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Poleaxe", Icons.bTNStrengthOfTheMoon, 58))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Halberd", Icons.bTNStrengthOfTheMoon, 59))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("War Scythe", Icons.bTNThorns, 60))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Lochaber Axe,", Icons.bTNStrengthOfTheMoon, 61))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Partizan", Icons.bTNStrengthOfTheMoon, 62))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Ogre Axe", Icons.bTNSpiritWalkerMasterTraining, 63))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Thresher", Icons.bTNWitchDoctorMaster, 64))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Cryptic Axe", Icons.bTNSpiritWalkerMasterTraining, 65))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Divine Scepter", Icons.bTNScepterOfMastery, 66))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Mighty Scepter", Icons.bTNScepterOfMastery, 67))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Trident", Icons.bTNImprovedStrengthOfTheMoon, 68))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Brandistock", Icons.bTNNagaWeaponUp1, 69))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Spetum", Icons.bTNSearingArrows, 70))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Pike", Icons.bTNImpalingBolt, 71))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Fuscina", Icons.bTNNagaWeaponUp3, 72))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("War Fork", Icons.bTNNagaWeaponUp3, 73))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Yari", Icons.bTNNagaWeaponUp3, 74))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Lance", Icons.bTNColdArrows, 75))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Battle Staff", Icons.bTNFlute, 76))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("War Staff", Icons.bTNFlute, 77))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Elder Staff", Icons.bTNFlute, 78))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Short Staff", Icons.bTNFlute, 79))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Long Staff", Icons.bTNFlute, 80))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Gnarled Staff", Icons.bTNFlute, 81))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Quarterstaff", Icons.bTNFlute, 82))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Cedar Staff", Icons.bTNFlute, 83))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Sabre", "ReplaceableTextures\\CommandButtons\\BTNSlaugther Saber.blp", 84))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Colossus Blade", Icons.bTNThoriumMelee, 85))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Scimitar", "ReplaceableTextures\\CommandButtons\\BTNSlaugther Saber.blp", 86))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Falchion", "ReplaceableTextures\\CommandButtons\\BTNSlaugther Saber.blp", 87))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Claymore", Icons.bTNThoriumMelee, 88))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Giant Sword", Icons.bTNSteelMelee, 89))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Two handed Sword", Icons.bTNSteelMelee, 90))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Bastard Sword", Icons.bTNSteelMelee, 91))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Flamberge", "ReplaceableTextures\\CommandButtons\\BTNSlaugther Saber.blp", 92))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Great Sword", Icons.bTNSteelMelee, 93))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Gladius", Icons.bTNThoriumMelee, 94))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Cutlass", "ReplaceableTextures\\CommandButtons\\BTNSlaugther Saber.blp", 95))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Shamshir", "ReplaceableTextures\\CommandButtons\\BTNSlaugther Saber.blp", 96))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Tulwar", "ReplaceableTextures\\CommandButtons\\BTNSlaugther Saber.blp", 97))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Espandon", "ReplaceableTextures\\CommandButtons\\BTNSlaugther Saber.blp", 98))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Dacian Falx", "ReplaceableTextures\\CommandButtons\\BTNSlaugther Saber.blp", 99))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Tusk Sword", Icons.bTNThoriumMelee, 100))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Zweihander", Icons.bTNThoriumMelee, 101))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Elegant Blade", Icons.bTNArcaniteMelee, 102))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Balrog Blade", Icons.bTNThoriumMelee, 103))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Champion Sword", Icons.bTNThoriumMelee, 104))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Military Pick", Icons.bTNImprovedMining, 105))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Kris", "ReplaceableTextures\\CommandButtons\\BTNGreen Dagger.blp", 106))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Pilum", Icons.bTNPoisonArrow, 107))
    ..put(compiletime(ITEM_ID_GEN.next()), new MeleeData("Hatchet Hands", Icons.bTNGlove, 108))
public constant melee_pool = new HashList<int>()

constant bow_qualifiers = [
    new WeaponQualifier("short ", .5),
    new WeaponQualifier("light ", .8),
    new WeaponQualifier("", 1.),
    new WeaponQualifier("long ", 1.1),
    new WeaponQualifier("heavy ", 1.2),
    new WeaponQualifier("siege ", 1.3)
]

constant melee_qualifiers = [
    new WeaponQualifier("broken ", .1),
    new WeaponQualifier("cracked ", .5),
    new WeaponQualifier("crude ", .6),
    new WeaponQualifier("damaged ", .7),
    new WeaponQualifier("low quality ", .75),
    new WeaponQualifier("lesser ", .85),
    new WeaponQualifier("", 1.),
    new WeaponQualifier("greater ", 1.1),
    new WeaponQualifier("superior ", 1.2),
    new WeaponQualifier("flawless ", 1.5)
]

public constant bow_damages = new HashMap<item, int>()
public constant melee_damages = new HashMap<item, int>()

function dropBow(unit forWhom, vec2 where) returns item
    let qualifier = bow_qualifiers[GetRandomInt(0, 5)]
    let bow = bow_pool.getRandomElement()
    let bonus = (bows.get(bow).power().toReal() * qualifier.bonus).toInt()
    let tooltip = "+" + bonus.toString() + " damage to archer attacks."
    let drop = createItem(bow, where)
        ..setDescription(tooltip)
        ..setExtendedTooltip("A " + qualifier.qualifier.color(COLOR_GOLD) + "bow. Provides " + tooltip)
    bow_damages.put(drop, bonus)

    cleanupItem(forWhom, drop)

    return drop

function dropMelee(unit forWhom, vec2 where) returns item
    let qualifier = melee_qualifiers[GetRandomInt(0, 9)]
    let melee = melee_pool.getRandomElement()
    let bonus = max((melees.get(melee).power().toReal() * qualifier.bonus), 1).toInt()
    let tooltip = "+" + bonus.toString() + " damage to rogue attacks."
    let extended = "A "
        + qualifier.qualifier.color(COLOR_GOLD)
        + melees.get(melee).name.toLowerCase()
        + ". Provides " + tooltip
    let drop = createItem(melee, where)
        ..setDescription(tooltip)
        ..setExtendedTooltip(extended)

    melee_damages.put(drop, bonus)

    cleanupItem(forWhom, drop)

    return drop

public function dropWeapon(unit forWhom, vec2 where) returns item
    if GetRandomInt(1, 10) <= 5
        return dropMelee(forWhom, where)
    return dropBow(forWhom, where)

init
    bows.forEach() (integer key, BowData value) ->
        for i = 1 to (15 - value.rank)
            bow_pool.add(key)

    nullTimer() ->
        melees.forEach() (integer key, MeleeData value) ->
            nullTimer() ->
                for i = 1 to (109 - value.rank)
                    melee_pool.add(key)

class BaseZombieDefinition extends UnitDefinition
    construct(int id)
        super(id, 'nzom')
        setSleeps(false)
        setSpeedBase(160)
        setAttack1CooldownTime(2.)
        setGoldBountyAwardedBase(0)
        setGoldBountyAwardedNumberofDice(0)
        setGoldBountyAwardedSidesperDie(0)


@compiletime function gen()
    new HeroPreset(ID_SYLVANAS, 'Hvwd', "Ranger")
    ..addProperName("Sylvanas")
    ..buildHero()
    ..setAttacksEnabled(0)
    ..setAnimationCastBackswing(0.1)
    ..setAnimationCastPoint(0.15)
    ..setHeroAbilities("")
    ..setNormalAbilities(
        commaList(ID_FLURRY, ID_ATTACK_RANGE, ID_ROGUE_ABILITIES, ID_SILVER_ARROWS, ID_VOLLEY, 'AInv')
    )
    ..setCanFlee(false)

    new HeroPreset(ID_KATYDMA, 'Ucrl', "Necromancer Lord")
    ..addProperName("Katydma")
    ..buildHero()
    ..setHitPointsMaximumBase(9001)
    ..setScalingValue(1.5)
    ..setModelFile("wardragon.mdx")
    ..setTintingColorBlue(127)
    ..setTintingColorRed(127)
    ..setTintingColorGreen(127)
    ..setAttacksEnabled(0)
    ..setAnimationCastBackswing(0.1)
    ..setAnimationCastPoint(0.15)
    ..setHeroAbilities("")
    ..setNormalAbilities("")
    ..setCanFlee(false)

    new HeroPreset(ID_KATYDMA_ALT, 'Ucrl', "Necromancer Lord")
    ..addProperName("Katydma")
    ..buildHero()
    ..setHitPointsMaximumBase(9001)
    ..setScalingValue(1.5)
    ..setModelFile("wardragon_alt.mdx")
    ..setTintingColorBlue(127)
    ..setTintingColorRed(127)
    ..setTintingColorGreen(127)
    ..setAttacksEnabled(0)
    ..setAnimationCastBackswing(0.1)
    ..setAnimationCastPoint(0.15)
    ..setHeroAbilities("")
    ..setNormalAbilities("")
    ..setCanFlee(false)

    new HeroPreset(ID_JAINA, 'Hjai', "Archmage")
    ..addProperName("Jaina")

    new BaseZombieDefinition(ID_ZOMBIE)
        ..setHitPointsMaximumBase(120)
    new BaseZombieDefinition(ID_ZOMBIE_FAT)
        ..setHitPointsMaximumBase(360)
        ..setSpeedBase(50)
        ..setSpeedMaximum(50)
        ..setSpeedMinimum(50)
        ..setScalingValue(1.25)
        ..setTintingColorBlue(155)
        ..setTintingColorGreen(155)
        ..setTintingColorRed(155)
        ..setAttack1DamageBase(40)

    bows.forEach() (integer key, BowData value) ->
        new ItemDefinition(key, 'ches')
            ..setName(value.name)
            ..setTooltipBasic(value.name)
            ..setTooltipExtended("A powerful bow.")
            ..setDescription("A powerful bow.")
            ..setInterfaceIcon(value.icon)

    melees.forEach() (integer key, MeleeData value) ->
        new ItemDefinition(key, 'ches')
            ..setName(value.name)
            ..setTooltipBasic(value.name)
            ..setTooltipExtended("A powerful weapon.")
            ..setDescription("A powerful weapon.")
            ..setInterfaceIcon(value.icon)
